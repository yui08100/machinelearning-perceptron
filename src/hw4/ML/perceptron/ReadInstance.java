package hw4.ML.perceptron;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadInstance {

	private List<short[]> AInstances;
	private List<short[]> BInstances;
	private List<short[]> CInstances;
	private List<short[]> DInstances;
	private List<short[]> EInstances;
	private List<short[]> FInstances;
	private List<short[]> GInstances;
	private List<short[]> HInstances;
	private List<short[]> IInstances;
	private List<short[]> JInstances;
	private List<short[]> KInstances;
	private List<short[]> LInstances;
	private List<short[]> MInstances;
	private List<short[]> NInstances;
	private List<short[]> OInstances;
	private List<short[]> PInstances;
	private List<short[]> QInstances;
	private List<short[]> RInstances;
	private List<short[]> SInstances;
	private List<short[]> TInstances;
	private List<short[]> UInstances;
	private List<short[]> VInstances;
	private List<short[]> WInstances;
	private List<short[]> XInstances;
	private List<short[]> YInstances;
	private List<short[]> ZInstances;

	public ReadInstance() {
		this.AInstances = new ArrayList<short[]>();
		this.BInstances = new ArrayList<short[]>();
		this.CInstances = new ArrayList<short[]>();
		this.DInstances = new ArrayList<short[]>();
		this.EInstances = new ArrayList<short[]>();
		this.FInstances = new ArrayList<short[]>();
		this.GInstances = new ArrayList<short[]>();
		this.HInstances = new ArrayList<short[]>();
		this.IInstances = new ArrayList<short[]>();
		this.JInstances = new ArrayList<short[]>();
		this.KInstances = new ArrayList<short[]>();
		this.LInstances = new ArrayList<short[]>();
		this.MInstances = new ArrayList<short[]>();
		this.NInstances = new ArrayList<short[]>();
		this.OInstances = new ArrayList<short[]>();
		this.PInstances = new ArrayList<short[]>();
		this.QInstances = new ArrayList<short[]>();
		this.RInstances = new ArrayList<short[]>();
		this.SInstances = new ArrayList<short[]>();
		this.TInstances = new ArrayList<short[]>();
		this.UInstances = new ArrayList<short[]>();
		this.VInstances = new ArrayList<short[]>();
		this.WInstances = new ArrayList<short[]>();
		this.XInstances = new ArrayList<short[]>();
		this.YInstances = new ArrayList<short[]>();
		this.ZInstances = new ArrayList<short[]>();
	}

	public void runInstance(FileReader inputData) {
		try (BufferedReader br = new BufferedReader(inputData)) {
			String line = br.readLine();
			while (line != null) {
				setSymbolRender(line);
				line = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setSymbolRender(String readLine) {
		if (readLine.startsWith("A")) {
			this.AInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("B")) {
			this.BInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("C")) {
			this.CInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("D")) {
			this.DInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("E")) {
			this.EInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("F")) {
			this.FInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("G")) {
			this.GInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("H")) {
			this.HInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("I")) {
			this.IInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("J")) {
			this.JInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("K")) {
			this.KInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("L")) {
			this.LInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("M")) {
			this.MInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("N")) {
			this.NInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("O")) {
			this.OInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("P")) {
			this.PInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("Q")) {
			this.QInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("R")) {
			this.RInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("S")) {
			this.SInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("T")) {
			this.TInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("U")) {
			this.UInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("V")) {
			this.VInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("W")) {
			this.WInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("X")) {
			this.XInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("Y")) {
			this.YInstances.add(saveAttributes(readLine));
		} else if (readLine.startsWith("Z")) {
			this.ZInstances.add(saveAttributes(readLine));
		}

	}

	private short[] saveAttributes(String readLine) {
		short[] attributes = new short[17] ;
		String[] array_attr = readLine.split(",");
		if(readLine.startsWith("A")) {
			attributes[0] = (short) 1;
		} else {
			attributes[0] = (short) -1;
		}
		for (int i = 1; i <= array_attr.length - 1; i++) {
			attributes[i] = Short.parseShort(array_attr[i]);
		}
		return attributes;
	}

	public List<short[]> getAInstances() {
		return AInstances;
	}

	public List<short[]> getBInstances() {
		return BInstances;
	}

	public List<short[]> getCInstances() {
		return CInstances;
	}

	public List<short[]> getDInstances() {
		return DInstances;
	}

	public List<short[]> getEInstances() {
		return EInstances;
	}

	public List<short[]> getFInstances() {
		return FInstances;
	}

	public List<short[]> getGInstances() {
		return GInstances;
	}

	public List<short[]> getHInstances() {
		return HInstances;
	}

	public List<short[]> getIInstances() {
		return IInstances;
	}

	public List<short[]> getJInstances() {
		return JInstances;
	}

	public List<short[]> getKInstances() {
		return KInstances;
	}

	public List<short[]> getLInstances() {
		return LInstances;
	}

	public List<short[]> getMInstances() {
		return MInstances;
	}

	public List<short[]> getNInstances() {
		return NInstances;
	}

	public List<short[]> getOInstances() {
		return OInstances;
	}

	public List<short[]> getPInstances() {
		return PInstances;
	}

	public List<short[]> getQInstances() {
		return QInstances;
	}

	public List<short[]> getRInstances() {
		return RInstances;
	}

	public List<short[]> getSInstances() {
		return SInstances;
	}

	public List<short[]> getTInstances() {
		return TInstances;
	}

	public List<short[]> getUInstances() {
		return UInstances;
	}

	public List<short[]> getVInstances() {
		return VInstances;
	}

	public List<short[]> getWInstances() {
		return WInstances;
	}

	public List<short[]> getXInstances() {
		return XInstances;
	}

	public List<short[]> getYInstances() {
		return YInstances;
	}

	public List<short[]> getZInstances() {
		return ZInstances;
	}
	
}
