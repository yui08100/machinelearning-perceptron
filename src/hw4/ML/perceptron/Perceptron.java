package hw4.ML.perceptron;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Perceptron {

	private float learningRate;
	private List<short[]> trainingList;
	private List<short[]> testingList;
	private float[] perceptronWeight;

	public Perceptron(Float learningRate, List<short[]> aInstances,
			List<short[]> otherAlphbetListRender) {
		this.learningRate = learningRate;
		this.trainingList = new ArrayList<short[]>();
		this.testingList = new ArrayList<short[]>();
		this.perceptronWeight = new float[17];
		readyToLearning(aInstances, otherAlphbetListRender);

	}

	private void readyToLearning(List<short[]> aInstances,
			List<short[]> otherAlphbetListRender) {
		for (int i = 0; i < aInstances.size(); i++) {
			if (i < aInstances.size() / 2) {
				this.trainingList.add(aInstances.get(i));
			} else {
				this.testingList.add(aInstances.get(i));
			}
		}

		for (int i = 0; i < otherAlphbetListRender.size(); i++) {
			if (i < otherAlphbetListRender.size() / 2) {
				this.trainingList.add(otherAlphbetListRender.get(i));
			} else {
				this.testingList.add(otherAlphbetListRender.get(i));
			}
		}

		Random ran = new Random();
		for (int i = 0; i < 17; i++) {
			this.perceptronWeight[i] = (ran.nextFloat() * (1.0f - (-1.0f)) + (-1.0f));
		}
	}

	public Boolean learning() {
		Boolean hasChange = false;
		for (short[] traingData : this.trainingList) {
			short output = ifPostive(traingData);
			if (traingData[0] == output) {
				continue;
			} else {
				this.perceptronWeight = newWeight(output,
						this.perceptronWeight, traingData);
				hasChange = true;
			}
		}
		return hasChange;
	}

	public int testingA() {
		int correctness = 0;
		for (int i = 0; i < 395; i++) {
			if (this.testingList.get(i)[0] == ifPostive(this.testingList.get(i))) {
				correctness++;
			}
		}
		return correctness;
	}

	public int testingOthers() {
		int correctness = 0;
		for (int i = 395; i < this.trainingList.size(); i++) {
			if (this.testingList.get(i)[0] == ifPostive(this.testingList.get(i))) {
				correctness++;
			}
		}
		return correctness;
	}

	private float[] newWeight(short output, float[] oldWeight,
			short[] wrongTraining) {
		float[] newWeight = new float[17];
		newWeight[0] = oldWeight[0]
				+ ((wrongTraining[0] - output) * this.learningRate * 1);
		for (int i = 1; i < 17; i++) {
			newWeight[i] = (oldWeight[i] + ((wrongTraining[0] - output)
					* (((float) wrongTraining[i]) / 15) * this.learningRate));
		}
		return newWeight;
	}

	private short ifPostive(short[] trainingData) {
		// System.out.println(this.perceptronWeight);
		float sum_of_weight_vector = this.perceptronWeight[0];
		for (int i = 1; i < 17; i++) {
			sum_of_weight_vector += (((float) trainingData[i]) / 15)
					* this.perceptronWeight[i];
		}
		if (sum_of_weight_vector > ((float) 0)) {
			return 1;
		} else {
			return -1;
		}
	}

	public float[] getPerceptronWeight() {
		return perceptronWeight;
	}

}
