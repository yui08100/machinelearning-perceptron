package hw4.ML.perceptron;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExerciseRender {

	private float learningRate;
	private List<String> alphbets;
	private ReadInstance read_instance;
	private FileReader files;

	public ExerciseRender(float learningRate, FileReader fileReader) {
		this.learningRate = learningRate;
		// other alphbets
		this.alphbets = new ArrayList<String>(Arrays.asList("B", "C", "D", "E",
				"F",  "G", "H", "I", "J", "K", "L", "M", "N", "O", 
				"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"));
		this.read_instance = new ReadInstance();
		this.files = fileReader;
	}

	public void run() throws FileNotFoundException {
		this.read_instance.runInstance(this.files);

		for (String alphbet : this.alphbets) {
			Perceptron perceptron = new Perceptron(this.learningRate,
					this.read_instance.getAInstances(),
					otherAlphbetListRender(alphbet));
			Boolean has_change = true;
			int epochs = 0;
			// if there has change in the epoch, will start a new epoch in the
			// end of the epoch
			while (has_change) {
				has_change = false;
				has_change |= perceptron.learning();
				epochs++;
				// System.out.println(perceptron.getPerceptronWeight().toString());
			}
			int testingAResult = perceptron.testingA();
			int testingOtherResult = perceptron.testingOthers();
			int testingOtherSize = (otherAlphbetListRender(alphbet).size() - (otherAlphbetListRender(
					alphbet).size() / 2));
			System.out.println("A v.s. " + alphbet);
			System.out.println("Epochs: " + epochs);
			System.out
					.println("Accuracy: ("
							+ String.valueOf(testingAResult)
							+ " + "
							+ String.valueOf(testingOtherResult)
							+ ") / ("
							+ String.valueOf(testingOtherSize)
							+ " + "
							+ 395
							+ ") = "
							+ ((float) (testingAResult + testingOtherResult) / (float) (testingOtherSize + 395)));
			System.out.println("Precision A: "
					+ String.valueOf(testingAResult)
					+ " / ("
					+ String.valueOf(testingAResult)
					+ " + "
					+ String.valueOf(testingOtherSize - testingOtherResult)
					+ ") = "
					+ ((float) testingAResult / (float) (testingAResult
							+ testingOtherSize - testingOtherResult)));
			System.out.println("Precision "
					+ alphbet
					+ ": "
					+ String.valueOf(testingOtherResult)
					+ " / ("
					+ String.valueOf(testingOtherResult)
					+ " + "
					+ String.valueOf(395 - testingAResult)
					+ ") = "
					+ ((float) testingOtherResult / (float) (testingOtherResult
							+ 395 - testingAResult)));

			System.out.println("Recall A: " + testingAResult + " / 395 = "
					+ (float) testingAResult / 395);
			System.out.println("Recall " + alphbet + ": " + testingOtherResult
					+ " / " + testingOtherSize + " = "
					+ (float) testingOtherResult / testingOtherSize);
			System.out.println();

		}
	}

	private List<short[]> otherAlphbetListRender(String negativeWord) {
		if (negativeWord.equals("A")) {
			return this.read_instance.getAInstances();
		} else if (negativeWord.equals("B")) {
			return this.read_instance.getBInstances();
		} else if (negativeWord.equals("C")) {
			return this.read_instance.getCInstances();
		} else if (negativeWord.equals("D")) {
			return this.read_instance.getDInstances();
		} else if (negativeWord.equals("E")) {
			return this.read_instance.getEInstances();
		} else if (negativeWord.equals("F")) {
			return this.read_instance.getFInstances();
		} else if (negativeWord.equals("G")) {
			return this.read_instance.getGInstances();
		} else if (negativeWord.equals("H")) {
			return this.read_instance.getHInstances();
		} else if (negativeWord.equals("I")) {
			return this.read_instance.getIInstances();
		} else if (negativeWord.equals("J")) {
			return this.read_instance.getJInstances();
		} else if (negativeWord.equals("K")) {
			return this.read_instance.getKInstances();
		} else if (negativeWord.equals("L")) {
			return this.read_instance.getLInstances();
		} else if (negativeWord.equals("M")) {
			return this.read_instance.getMInstances();
		} else if (negativeWord.equals("N")) {
			return this.read_instance.getNInstances();
		} else if (negativeWord.equals("O")) {
			return this.read_instance.getOInstances();
		} else if (negativeWord.equals("P")) {
			return this.read_instance.getPInstances();
		} else if (negativeWord.equals("Q")) {
			return this.read_instance.getQInstances();
		} else if (negativeWord.equals("R")) {
			return this.read_instance.getRInstances();
		} else if (negativeWord.equals("S")) {
			return this.read_instance.getSInstances();
		} else if (negativeWord.equals("T")) {
			return this.read_instance.getTInstances();
		} else if (negativeWord.equals("U")) {
			return this.read_instance.getUInstances();
		} else if (negativeWord.equals("V")) {
			return this.read_instance.getVInstances();
		} else if (negativeWord.equals("W")) {
			return this.read_instance.getWInstances();
		} else if (negativeWord.equals("X")) {
			return this.read_instance.getXInstances();
		} else if (negativeWord.equals("Y")) {
			return this.read_instance.getYInstances();
		} else if (negativeWord.equals("Z")) {
			return this.read_instance.getZInstances();
		}
		return null;
	}
}
