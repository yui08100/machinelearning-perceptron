package hw4.ML.perceptron;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Exercise1 {

	public static void main(String[] args) {
		try {
			ExerciseRender ex = new ExerciseRender((float) 0.2, new FileReader(
					"letter-recognition.data/letter-recognition.data"));
			ex.run();
		} catch (FileNotFoundException e) {
			System.err.print("letter-recognition.data not found");
			e.printStackTrace();
		}

	}

}
